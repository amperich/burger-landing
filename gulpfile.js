const gulp = require('gulp');

//scss
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
//const groupMediaQueries = require('gulp-group-css-media-queries');
const csso = require('gulp-csso');
const autoprefixer = require('gulp-autoprefixer');

//js
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

//вспомогательные плагины
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const del = require('del');

//пути app - разработка, build - готовая сборка

const paths = {
    app: './app/',
    build: './dist/'
};

//html

gulp.task('htmls', function () {
    return gulp.src(paths.app + '*.html')
        .pipe(gulp.dest(paths.build));
});

//scss

gulp.task('styles', function () {
    return gulp.src(paths.app + 'scss/main.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass())
        .pipe(autoprefixer(['last 2 versions'], { cascade: false }))
        //.pipe(groupMediaQueries())
        .pipe(csso())
        .pipe(rename({ suffix: ".min"}))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(paths.build + 'css/'));
});

//js

gulp.task('js', function () {
    return gulp.src([paths.app + 'js/**/*.js', '!./app/js/libs/**/*'])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(concat('script.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.build + 'js/'))
});

//js libs

gulp.task('libsJs', function () {
   return gulp.src(paths.app + 'js/libs/*')
       .pipe(gulp.dest(paths.build + 'js/libs/'));
});

//images min

gulp.task('images', function () {
    return gulp.src(paths.app + 'images/**/*')
        .pipe(gulp.dest(paths.build + 'images/'))
});

//fonts

gulp.task('fonts', function () {
    return gulp.src(paths.app + 'fonts/**/*')
        .pipe(gulp.dest(paths.build + 'fonts/'))
});

//php

gulp.task('php', function () {
    return gulp.src(paths.app + '*.php')
        .pipe(gulp.dest(paths.build))
});

//browser-sync

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: paths.build
        }
    });
    browserSync.watch(paths.build + '**/*.*').on('change', browserSync.reload);
});

//очистка

gulp.task('clean', function () {
    return del('dist/')
});

//watch pug, scss, js

gulp.task('watch', function () {
    gulp.watch( paths.app + 'pug/**/*.pug', gulp.series('htmls'));
    gulp.watch( paths.app + 'scss/**/*.scss', gulp.series('styles'));
    gulp.watch( paths.app + 'js/**/*.js', gulp.series('js'));
});

//gulp

gulp.task('default', gulp.series(
    'clean',
    gulp.parallel('htmls', 'styles', 'js', 'libsJs', 'images', 'fonts', 'php'),
    gulp.parallel('watch', 'serve')
));